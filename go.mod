module gitlab.com/gitlab-org/labkit

require (
	github.com/certifi/gocertifi v0.0.0-20180905225744-ee1a9a0726d2 // indirect
	github.com/client9/reopen v1.0.0
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/getsentry/raven-go v0.1.0
	github.com/gogo/protobuf v1.1.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lightstep/lightstep-tracer-go v0.15.6
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/opentracing/opentracing-go v1.0.2
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	github.com/tinylib/msgp v1.0.2 // indirect
	github.com/uber-go/atomic v1.3.2 // indirect
	github.com/uber/jaeger-client-go v2.15.0+incompatible
	github.com/uber/jaeger-lib v1.5.0 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25 // indirect
	golang.org/x/sys v0.0.0-20190306220723-b294cbcfc56d // indirect
	google.golang.org/grpc v1.16.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.7.0
)
