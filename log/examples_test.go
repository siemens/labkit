package log_test

import (
	"gitlab.com/gitlab-org/labkit/log"
)

func ExampleInitialize() {
	// Initialize the global logger
	closer, err := log.Initialize(
		log.WithFormatter("json"),    // Use JSON formatting
		log.WithLogLevel("info"),     // Use info level
		log.WithOutputName("stderr"), // Output to stderr
	)
	defer closer.Close()
	log.WithError(err).Info("This has been logged")

	// Example of configuring a non-global logger using labkit
	myLogger := log.New() // New logrus logger
	closer2, err := log.Initialize(
		log.WithLogger(myLogger),                  // Tell logkit to initialize myLogger
		log.WithFormatter("text"),                 // Use text formatting
		log.WithLogLevel("debug"),                 // Use debug level
		log.WithOutputName("/var/log/labkit.log"), // Output to `/var/log/labkit.log`
	)
	defer closer2.Close()

	log.WithError(err).Info("This has been logged")
}
